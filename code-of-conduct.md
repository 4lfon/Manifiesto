Código de conducta
==================

Coruña Hacks es un evento hecho por la comunidad pensada para la colaboración dentro de la comunidad de desarrolladores.

La organización de Coruña Hacks está comprometida con la creación de un espacio seguro y cómodo para discutir ideas relacionadas con la programación, el desarrollo, el software libre y el resto de temas que se aborden durante los eventos.


Valoramos la participación de **todos** los miembros de la comunidad y queremos que todos los asistentes tengan una experiencia entretenida y satisfactoria. Para ello, se espera que todos los asistentes muestren respeto y cortesía hacia el resto durante las jornadas y todos los eventos relacionados con ella. Por ello, animamos a todos los participantes a acercarse al evento con actitudes abiertas y positivas, y a interactuar con los demás de forma constructiva.

El presente código de conducta es de aplicación a los Hackatones de Coruña Hacks, así como todos aquellos eventos sociales relacionados con las actividades, como almuerzos o reuniones en restaurantes o bares.

Es muy enriquecedor que las discusiones incluyan opiniones y experiencias diversas, así que no se tolerará ningún tipo de acoso. Se tomarán acciones de respuesta ante acosos relacionados a nacionalidaes, género, identidad y expresión de género, raza, etnia, orientación sexual, características físicas, discapacidades, religión, o edad.



Comportamientos inaceptables
----------------------------

Los comportamientos de acoso pueden ocurrir en linea o en persona. Son ejemplos de comportamientos inaceptables los siguientes:

1. Comentarios verbales que refuerzan la opresión relacionada al género, identidad y expresión del género, orientación sexual, discapacidad, apariencia física, tamaño corporal, raza, etnia, edad o religión (o su falta);

2. Amenazas verbales;

3. Imagenes sexualizadas en espacios públicos;

4. La intimidación deliberada;

5. El acecho o persecución;

6. El acoso mediante fotografías o grabaciones;

7. La constante inrerrupción de sesiones, charlas o de la participación en público de otro asistente;

8. El contacto físico o propuesta sexual inapropiados;

9. La introducción de drogas en la comida o bebida;

10. Solicitar la ayuda de otros, en persona o en línea, con el fin de acosar a un participante;

11. Las asunciones de género y vigilancia;

12. Defender, propugnar o alentar cualquiera de los comportamientos anteriores.

Ha de dejarse a alguien abandonar una conversación que le haga sentirse incómodo, y no se ha de seguir a quien haya pedido estar solo. Si discutes asuntos que puedan ser traumáticos para los participantes, provee advertencias para que las demás personas puedan marcharse de la conversación o planear en estrategias para lidiar con ello.

Aquellos participantes que sean apercibidos ante alguno de estos comportamientos deberán desistir inmediatamente de su actitud. Dependiendo de la severidad o de la reincidencia en su comportamiento, la organización podrá responder escaladamente, incluyendo, entre otros, la advertencia o expulsión del evento.

El personal de la conferencia ayudará gustosamente a los participantes a que se pongan en contacto con el personal de seguridad del lugar o la policía local, y proporcionará escolta o socorrerá de cualquier otra forma a las víctimas de acoso para que puedan sentirse seguras durante el desarrollo de la conferencia. Tu bienestar es una de nuestras prioridades.

Contamos con todo el mundo para asegurar experiencias positivas entre todos. Si estás siendo objeto de acoso, o adviertes que otra persona lo esté siendo, o tienes cualquier otra preocupación, por favor contacta con la organización inmediatamente.

Para información de contacto baja hasta *En caso de problemas* debajo.


En caso de problemas
--------------------

Se puede informar confidencialmente de cualquier problema respecto a este código de conducta de las siguientes formas:

1. Localizando a un organizador de Coruña Hacks en persona. Al inicio de la conferencia serán fácilmente identificables, y a lo largo de ella, serán quienes hagan los distintos anuncios que vayan ocurriendo.

2. Enviando un email a corunahacks@gpul.org


Información local de emergencias
--------------------------------

Para las actividades que se realicen presencialmente, ante todo está el **número de emergencias europeo**: [112](tel:112)

112 es el número de emergencias europeo y se puede utilizar gratuitamente desde cualquier conexión telefónica de la Unión Europea. Se puede llamar al 112 desde fijos y móviles para contactar a cualquier servicio de emergencia: ambulancias, bomberos y policía.

Al llamar al 112 la [Axencia Galega de Emerxencias](http://www.axega112.org/es) recibirá tu llamada y podrá ponerte en contacto con un operador específico tras comunicar tu incidencia.


Agradecimientos
---------------

Gracias por hacer de este un evento amistoso y abierto a la comunidad.


Licencia
--------

Este Código de Conducta se basa en el códigos de conducta de PyCon ES
2017 (https://2017.es.pycon.org/es/code-of-conduct/) y está disponible
bajo una licencia Creative Commons Atribución 3.0 España.
